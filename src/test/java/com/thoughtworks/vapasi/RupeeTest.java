package com.thoughtworks.vapasi;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.ParameterizedTest;

public class RupeeTest {
    @Test
    void shouldReturnTrueIfValuesAreEqual(){
        assertEquals(
                new Rupee(10),
                new Rupee(10)
        );
    }
    @Test
    void shouldReturnFalseIfValuesAreNotEqual(){
        assertNotEquals(
                new Rupee(5),
                new Rupee(10)
        );
    }
    @Test
    void shouldReturnTrueIfAddedValuesAreEqual(){
        assertEquals(
                new Rupee(5).add(new Rupee(5)),
                new Rupee(10)
        );
    }
    @Test
    void shouldReturnFalseIfAddedValuesAreNotEqual(){
        assertNotEquals(
                new Rupee(10),
                new Rupee(5).add(new Rupee(4))

        );
    }
}
