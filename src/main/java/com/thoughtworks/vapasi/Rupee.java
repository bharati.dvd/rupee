package com.thoughtworks.vapasi;

import java.util.Objects;

public class Rupee {
    int denomination;
    public Rupee(int denomination){
        this.denomination = denomination;
    }
    public Rupee add(Rupee other){
        return new Rupee(denomination+ other.denomination);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return false;
        if (o == null || getClass() != o.getClass()) return false;
        Rupee rupee = (Rupee) o;
        return denomination == rupee.denomination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination);
    }
}
